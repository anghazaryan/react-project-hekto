/* eslint-disable no-unused-vars */
import PropTypes from "prop-types";
import { createContext, useEffect, useReducer } from "react";
import { sortProducts } from "./features/sortProducts";
import { checkItems } from "./features/checkItems";
import { unCheckItems } from "./features/unCheckItems";
import { fetchFilters } from "./features/fetchFilters";
import { findProduct } from "./features/findProduct";

const ProductContext = createContext({
  fetchedProducts: [],
  filters: {},
  searchedProducts: [],
  checkedItems: [],
  findProducts: (productName) => {},
  sortProducts: (sortRule) => {},
  addCheckedItems: (productCategory, productLabel) => {},
  removeUncheckedItems: (productCategory, productLabel) => {},
});

const productReducer = (state, action) => {
  if (action.type === "FETCH_PRODUCTS") {
    return { ...state, fetchedProducts: action.products };
  }

  if (action.type === "FETCH_FILTERS") {
    return fetchFilters(state, action);
  }

  if (action.type === "FIND_PRODUCT") {
    return findProduct(state, action);
  }

  if (action.type === "SORT_PRODUCTS") {
    sortProducts(state, action);
  }

  if (action.type === "ADD_CHECKED_ITEMS") {
    return checkItems(state, action);
  }

  if (action.type === "REMOVE_UNCHECKED_ITEMS") {
    return unCheckItems(state, action);
  }

  return state;
};

export const ProductContextProvider = ({ children }) => {
  const [product, dispatchProductAction] = useReducer(productReducer, {
    fetchedProducts: [],
    filters: {},
    searchedProducts: [],
    checkedItems: [],
  });

  useEffect(() => {
    async function fetchProducts() {
      try {
        const response = await fetch("http://localhost:3000/products");

        if (!response.ok) {
          throw new Error("Failed to fetch products");
        }

        const products = await response.json();

        dispatchProductAction({ type: "FETCH_PRODUCTS", products: products });
      } catch (error) {
        console.error("Error fetching products:", error.message);
      }
    }

    async function fetchFilters() {
      try {
        const response = await fetch("http://localhost:3000/filters");

        if (!response.ok) {
          throw new Error("Failed to fetch filters");
        }

        const filters = await response.json();
        dispatchProductAction({ type: "FETCH_FILTERS", filters: filters });
      } catch (error) {
        console.log("Error fetching filters", error.message);
      }
    }
    fetchProducts();
    fetchFilters();
  }, []);

  const findProducts = (productName) => {
    dispatchProductAction({ type: "FIND_PRODUCT", productName });
  };

  const sortProducts = (sortRule) => {
    dispatchProductAction({ type: "SORT_PRODUCTS", sortRule });
  };

  const addCheckedItems = (productCategory, productLabel) => {
    dispatchProductAction({
      type: "ADD_CHECKED_ITEMS",
      productCategory,
      productLabel,
    });
  };

  const removeUncheckedItems = (productCategory, productLabel) => {
    dispatchProductAction({
      type: "REMOVE_UNCHECKED_ITEMS",
      productCategory,
      productLabel,
    });
  };

  const productContext = {
    fetchedProducts: product.fetchedProducts,
    filters: product.filters,
    searchedProducts: product.searchedProducts,
    checkedItems: product.checkedItems,
    findProducts,
    sortProducts,
    addCheckedItems,
    removeUncheckedItems,
  };

  return (
    <ProductContext.Provider value={productContext}>
      {children}
    </ProductContext.Provider>
  );
};

ProductContextProvider.propTypes = {
  children: PropTypes.node,
};

export default ProductContext;
