export const unCheckItems = (state, action) => {
  const productCategory = action.productCategory;
  const productLabel = action.productLabel;
  let uncheckedItems = [];

  const newFilters = { ...state.filters };
  newFilters[productCategory]["values"].forEach((valueObj) => {
    if (valueObj.value === productLabel) {
      valueObj.isChecked = false;
    }
  });

  if (
    productCategory === "brand" ||
    productCategory === "category" ||
    productCategory === "discountPercentage"
  ) {
    const filteredItems = state.checkedItems.filter(
      (product) => product[productCategory] !== productLabel
    );

    uncheckedItems = [...uncheckedItems, ...filteredItems];
  }

  if (productCategory === "price") {
    const filteredItems = state.checkedItems.filter((product) => {
      switch (productLabel) {
        case 0:
          return !(product.price >= 0 && product.price < 150);
        case 150:
          return !(product.price >= 150 && product.price < 350);
        case 350:
          return !(product.price >= 350 && product.price < 550);
        case 550:
          return !(product.price >= 550 && product.price < 550);
        default:
          return !(product.price >= 850);
      }
    });

    uncheckedItems = [...uncheckedItems, ...filteredItems];
  }

  if (productCategory === "rating") {
    const filteredItems = state.checkedItems.filter((product) => {
      return !(Math.round(product.rating.value) === 6 - productLabel);
    });

    uncheckedItems = [...uncheckedItems, ...filteredItems];
  }

  return { ...state, checkedItems: uncheckedItems, filters: newFilters };
};
