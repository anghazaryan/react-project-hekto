export const fetchFilters = (state, action) => {
  const newFilters = {};

  Object.keys(action.filters).forEach((filter) => {
    if (filter !== "colors") {
      newFilters[filter] = {};
    }
  });

  Object.keys(newFilters).forEach((filter) => {
    newFilters[filter]["type"] = action.filters[filter]["type"];
    newFilters[filter]["label"] = action.filters[filter]["label"];

    if (filter === "price") {
      newFilters[filter]["values"] = [
        { value: 0, isChecked: false },
        { value: 150, isChecked: false },
        { value: 350, isChecked: false },
        { value: 550, isChecked: false },
        { value: 800, isChecked: false },
      ];
    } else {
      const values = action.filters[filter]["values"]
        .slice(0, 7)
        .map((value) => ({ value, isChecked: false }));
      newFilters[filter]["values"] = values;
    }

    if (filter === "brand") {
      newFilters[filter]["color"] = "var(--info-color)";
    } else if (filter === "rating") {
      newFilters[filter]["color"] = "var(--secondary-color)";
    } else {
      newFilters[filter]["color"] = "var(--primary-color)";
    }
  });

  return { ...state, filters: newFilters };
};
