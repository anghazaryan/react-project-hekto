import { resetFilters } from "../../utils/resetFilters";

export const findProduct = (state, action) => {
  const productName = action.productName.trim().toLowerCase();
  const filteredItems = state.fetchedProducts.filter((product) =>
    product.name.toLowerCase().includes(productName)
  );

  const newFilters = resetFilters(state.filters);

  return {
    ...state,
    searchedProducts: filteredItems,
    checkedItems: [],
    filters: newFilters,
  };
};
