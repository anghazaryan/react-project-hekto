export const sortProducts = (state, action) => {
  const sortedProducts =
    state.checkedItems && state.checkedItems.length > 0
      ? state.checkedItems
      : state.searchedProducts && state.searchedProducts.length > 0
      ? state.searchedProducts
      : state.fetchedProducts;
  if (
    action.sortRule === "Price: High -> Low" ||
    action.sortRule === "Price: Low -> High"
  ) {
    action.sortRule === "Price: High -> Low"
      ? sortedProducts.sort(
          (product1, product2) => product2.price - product1.price
        )
      : sortedProducts.sort(
          (product1, product2) => product1.price - product2.price
        );
  }
};
