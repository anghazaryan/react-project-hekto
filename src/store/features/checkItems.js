export const checkItems = (state, action) => {
  const productCategory = action.productCategory;
  const productLabel = action.productLabel;
  let checkedItems = [...state.checkedItems];

  const newFilters = { ...state.filters };
  newFilters[productCategory]["values"].forEach((valueObj) => {
    if (valueObj.value === productLabel) {
      valueObj.isChecked = true;
    }
  });

  const products =
    state.searchedProducts && state.searchedProducts.length > 0
      ? [...state.searchedProducts]
      : [...state.fetchedProducts];

  if (
    productCategory === "brand" ||
    productCategory === "category" ||
    productCategory === "discountPercentage"
  ) {
    const filteredItems = products.filter(
      (product) => product[productCategory] === productLabel
    );

    checkedItems = [...checkedItems, ...filteredItems];
  }

  if (productCategory === "price") {
    const filteredItems = products.filter((product) => {
      switch (productLabel) {
        case 0:
          return product.price >= 0 && product.price < 150;
        case 150:
          return product.price >= 150 && product.price < 350;
        case 350:
          return product.price >= 350 && product.price < 550;
        case 550:
          return product.price >= 550 && product.price < 550;
        default:
          return product.price >= 850;
      }
    });

    checkedItems = [...checkedItems, ...filteredItems];
  }

  if (productCategory === "rating") {
    const filteredItems = products.filter((product) => {
      return Math.round(product.rating.value) === 6 - productLabel;
    });

    checkedItems = [...checkedItems, ...filteredItems];
  }

  return { ...state, checkedItems, filters: newFilters };
};
