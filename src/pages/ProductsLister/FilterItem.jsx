import PropTypes from "prop-types";
import CustomCheckbox from "../../components/CustomCheckbox/CustomCheckbox";
import idGen from "../../utils/idGenerator";
import classes from "./FilterItem.module.css";
import RatingStars from "../../components/RatingStars/RatingStars";
import { useContext } from "react";
import ProductContext from "../../store/ProductContext";

const FilterItem = ({ type, title, labels, color }) => {
  const productCtx = useContext(ProductContext);

  let additionalLabel = "";

  if (type === "discountPercentage") {
    additionalLabel = " % Cashback";
  } else if (type === "price") {
    additionalLabel = "+";
  }

  const handleCheckboxCheck = (label) => {
    productCtx.addCheckedItems(type, label);
  };

  const handleCheckboxUncheck = (label) => {
    productCtx.removeUncheckedItems(type, label);
  };

  return (
    <div className={classes.filter}>
      <h3 className={classes.title}>{title}</h3>

      {labels.map((label) => {
        return (
          <CustomCheckbox
            label={
              type === "rating" ? (
                <RatingStars rating={6 - label.value} />
              ) : (
                `${label.value}${additionalLabel}`
              )
            }
            color={color}
            checked={label.isChecked}
            key={idGen()}
            id={`custom-checkbox-${idGen()}`}
            onCheck={() => handleCheckboxCheck(label.value)}
            onUncheck={() => handleCheckboxUncheck(label.value)}
          />
        );
      })}
    </div>
  );
};

FilterItem.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  color: PropTypes.string,
  labels: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool])
    )
  ),
};

export default FilterItem;
