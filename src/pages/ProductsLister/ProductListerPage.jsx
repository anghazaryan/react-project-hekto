import { NavLink } from "react-router-dom";
import classes from "./ProductListerPage.module.css";
import SvgIcon from "../../components/SvgIcon";
import CustomSelect from "../../components/CustomSelect/CustomSelect";
import FilterItem from "./FilterItem";
import idGen from "../../utils/idGenerator";
import Card6 from "../../components/Cards/Card6/Card6";
import { useContext, useState } from "react";
import ProductContext from "../../store/ProductContext";
import Pagination from "../../components/Pagination/Pagination";
import { PAGE_LIST } from "../../utils/constants";
import { SORT_LIST } from "../../utils/constants";

const ProductListerPage = () => {
  const [view, setView] = useState("list");
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);
  const [sortRule, setSortRule] = useState(SORT_LIST[0]);
  const productCtx = useContext(ProductContext);

  const { fetchedProducts, filters, searchedProducts, checkedItems } =
    productCtx;

  const filteredProducts =
    checkedItems && checkedItems.length > 0
      ? checkedItems
      : searchedProducts && searchedProducts.length > 0
      ? searchedProducts
      : fetchedProducts;

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = filteredProducts.slice(
    indexOfFirstPost,
    indexOfLastPost
  );

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const handleListView = () => {
    setView("list");
  };

  const handleGridView = () => {
    setView("grid");
  };

  const handlePostsPerPageClick = (number) => {
    setPostsPerPage(number);
  };

  const handleProductsSort = (sortRule) => {
    setSortRule(sortRule);
    productCtx.sortProducts(sortRule);
  };

  return (
    <section className={classes["products-page"]}>
      <h1 className={classes.heading}>Products</h1>
      <nav className={classes.nav}>
        <ul className={classes.list}>
          <li className={classes["list-item"]}>
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Home
            </NavLink>
          </li>
          <li>
            <SvgIcon name="icon_circle_small" width={4} height={4} />
          </li>
          <li className={classes["list-item"]}>
            <NavLink
              to="/products"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Products
            </NavLink>
          </li>
        </ul>
      </nav>
      <div className={classes.sort}>
        <div className={classes["sort-column"]}>
          <span className={classes["sort-title"]}>Per Page</span>
          <CustomSelect
            dropdownList={PAGE_LIST}
            className={classes["sort-select"]}
            onItemClick={handlePostsPerPageClick}
            selected={postsPerPage}
          />
        </div>
        <div className={classes["sort-column"]}>
          <span className={classes["sort-title"]}>Sort By</span>
          <CustomSelect
            dropdownList={SORT_LIST}
            className={classes["sort-select"]}
            onItemClick={handleProductsSort}
            selected={sortRule}
            width="21.8rem"
          />
        </div>
        <div className={classes["sort-column"]}>
          <span className={classes["sort-title"]}>View</span>
          <SvgIcon
            name={`icon_grid${view === "grid" ? "_active" : ""}`}
            width={24}
            height={24}
            onClick={handleGridView}
            className={view === "grid" ? classes["sort-icon"] : ""}
          />
          <SvgIcon
            name={`icon_list${view === "list" ? "_active" : ""}`}
            width={24}
            height={24}
            onClick={handleListView}
            className={view === "list" ? classes["sort-icon"] : ""}
          />
        </div>
      </div>
      <div className={classes["filter-and-products"]}>
        <div className={classes.filter}>
          {Object.keys(filters).map((item) => (
            <FilterItem
              type={item}
              title={filters[item]["label"]}
              labels={filters[item]["values"]}
              color={filters[item]["color"]}
              key={idGen()}
            />
          ))}
        </div>
        <div className={classes[`products-${view}`]}>
          {currentPosts.map((product) => (
            <Card6
              product={product}
              type={view === "list" ? "horizontal" : "vertical"}
              key={idGen()}
            />
          ))}
        </div>
      </div>
      <Pagination
        postsPerPage={postsPerPage}
        totalPosts={filteredProducts.length}
        currentPage={currentPage}
        paginate={paginate}
      />
    </section>
  );
};

export default ProductListerPage;
