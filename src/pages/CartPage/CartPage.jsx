import classes from "./CartPage.module.css";
import NumberInput from "../../components/NumberInput/NumberInput";
import Button from "../../components/Button/Button";
import { useContext, useEffect } from "react";
import CartContext from "../../store/CartContext";
import { currencyFormatter } from "../../utils/fortmatting";
import { useNavigate } from "react-router-dom";
import idGen from "../../utils/idGenerator";

const CartPage = () => {
  const cartCtx = useContext(CartContext);
  const navigate = useNavigate();

  const { items } = cartCtx;

  useEffect(() => {
    if (items.length === 0) {
      navigate("/cartEmpty");
    }
  }, [items, navigate]);

  const subtotal = items.reduce((subtotalPrice, item) => {
    return subtotalPrice + item.wasPrice * item.quantity;
  }, 0);

  const total = items.reduce((total, item) => {
    return total + item.price * item.quantity;
  }, 0);

  const handleCartClear = () => {
    navigate("/cartEmpty");
    cartCtx.removeAll();
  };

  const handleAddProduct = (item) => {
    cartCtx.addItem(item);
  };

  const handleRemoveProduct = (id) => {
    cartCtx.removeItem(id);
  };

  const handleProductClick = (productId) => {
    navigate(`/products/${productId}`);
  };

  return (
    <section className={classes.cart}>
      <div className={classes.container}>
        <div className={classes.products}>
          {items.map((item) => (
            <div className={classes.product} key={idGen()}>
              <img
                src={item.thumbnail}
                width={149}
                height={104}
                alt={item.name}
                className={classes.img}
                onClick={() => handleProductClick(item.id)}
              />
              <div className={classes["name-and-price"]}>
                <p className={classes["product-name"]}>{item.name}</p>
                <p className={classes["product-price"]}>
                  {currencyFormatter.format(item.price)}
                </p>
              </div>
              <div className={classes["number-input-and-price"]}>
                <NumberInput
                  count={item.quantity}
                  className={classes.quantity}
                  onMinusClick={() => handleRemoveProduct(item.id)}
                  onPlusClick={() => handleAddProduct(item)}
                />
                <p className={classes["product-price"]}>
                  {currencyFormatter.format(item.price * item.quantity)}
                </p>
              </div>
            </div>
          ))}
        </div>
        <div className={classes.summary}>
          <div className={classes.checkout}>
            <div className={classes.total}>
              <span className={classes.label}>Subtotal:</span>
              <span className={classes.price}>
                {currencyFormatter.format(subtotal)}
              </span>
            </div>
            <div className={classes.total}>
              <span className={classes.label}>Total:</span>
              <span className={classes.price}>
                {currencyFormatter.format(total)}
              </span>
            </div>
            <div className={classes.shipping}>
              <span className={classes["shipping-label"]}>Shipping:</span>
              <span className={classes["shipping-price"]}>$100.00</span>
            </div>
            <Button
              name="Proceed to checkout"
              size="medium"
              className={classes.btn}
            />
          </div>
          <button className={classes["btn-clear"]} onClick={handleCartClear}>
            Clear cart
          </button>
        </div>
      </div>
    </section>
  );
};

export default CartPage;
