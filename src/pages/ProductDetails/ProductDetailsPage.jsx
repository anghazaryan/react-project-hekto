import classes from "./ProductDetailsPage.module.css";
import SvgIcon from "../../components/SvgIcon";
import idGen from "../../utils/idGenerator";
import Button from "../../components/Button/Button";
import Tabs from "./Tabs";
import RelatedProducts from "../../components/Sections/RelatedProducts/RelatedProducts";
import { useContext } from "react";
import { useParams } from "react-router-dom";
import { currencyFormatter } from "../../utils/fortmatting";
import { ratingCounter } from "../../utils/ratingCounter";
import ProductDetailsImages from "./ProductDetailsImages";
import ProductContext from "../../store/ProductContext";
import useCart from "../../hooks/handleCartIconClick";

const ProductDetailsPage = () => {
  const productCtx = useContext(ProductContext);
  const { handleCartIconClick: handleAddProductToCart } = useCart();
  const { id } = useParams();

  const product = productCtx.fetchedProducts.find((prod) => prod.id === id);
  const [ratedStars, notRatedStars] = ratingCounter(product?.rating.value);

  if (productCtx.fetchedProducts.length === 0) {
    return <h1 className={classes.loading}>Loading...</h1>;
  }

  if (!product) {
    return <h1>Product not found!</h1>;
  }

  return (
    <>
      <section className={classes["section-product-view"]}>
        <ProductDetailsImages product={product} />
        <div className={classes.description}>
          <h3 className={classes.title}>{product.name}</h3>
          <div className={classes.rating}>
            {Array.from({ length: ratedStars }, () => (
              <SvgIcon
                name="icon_star"
                width={24}
                height={24}
                className={classes["icon-star-filled"]}
                key={idGen()}
              />
            ))}
            {Array.from({ length: notRatedStars }, () => (
              <SvgIcon
                name="icon_star"
                width={24}
                height={24}
                className={classes["icon-star"]}
                key={idGen()}
              />
            ))}
          </div>
          <div className={classes.prices}>
            <span className={classes.discountPrice}>
              {currencyFormatter.format(product.price)}
            </span>
            <span className={classes.price}>
              {currencyFormatter.format(product.wasPrice)}
            </span>
          </div>
          <p className={classes.text}>{product.description}</p>
          <div className={classes["add-to-cart"]}>
            <Button
              name="Add to Cart"
              size="medium"
              className={classes.btn}
              onClick={() => handleAddProductToCart(product)}
            />
            <SvgIcon name="icon_heart" width={24} height={24} />
          </div>
        </div>
      </section>
      <Tabs />
      <RelatedProducts product={product} />
    </>
  );
};

export default ProductDetailsPage;
