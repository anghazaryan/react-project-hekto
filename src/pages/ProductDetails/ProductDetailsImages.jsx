import PropTypes from "prop-types";
import classes from "./ProductDetailsImages.module.css";

const ProductDetailsImages = ({ product }) => {
  return (
    <div className={classes["images"]}>
      <div className={classes["images-left"]}>
        <img
          src={product.imageSet[0]}
          alt={product.name}
          width={172}
          height={122}
          className={classes.img}
        />
        <img
          src={product.imageSet[1]}
          alt={product.name}
          width={172}
          height={122}
          className={classes.img}
        />
        <img
          src={product.imageSet[2]}
          alt={product.name}
          width={172}
          height={122}
          className={classes.img}
        />
      </div>
      <div className={classes["images-right"]}>
        <img
          src={product.thumbnail}
          alt={product.name}
          width={475}
          height={394}
          className={`${classes.img} ${classes["img-right"]}`}
        />
      </div>
    </div>
  );
};

ProductDetailsImages.propTypes = {
  product: PropTypes.shape({
    imageSet: PropTypes.arrayOf(PropTypes.string),
    thumbnail: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
};

export default ProductDetailsImages;
