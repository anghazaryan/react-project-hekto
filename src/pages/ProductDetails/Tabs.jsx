import { useState } from "react";
import SvgIcon from "../../components/SvgIcon";
import idGen from "../../utils/idGenerator";
import classes from "./Tabs.module.css";
import { TAB_CONTENT } from "../../utils/constants";

const Tabs = () => {
  const [selectedTab, setSelectedTab] = useState("Description");

  let tabContent = TAB_CONTENT[selectedTab];

  const handleTabSelection = (topic) => {
    setSelectedTab(topic);
  };

  return (
    <section className={classes["section-product-details"]}>
      <div className={classes.container}>
        <ul className={classes.tabs}>
          {Object.keys(TAB_CONTENT).map((tab) => (
            <li
              className={`${classes.tab} ${
                selectedTab === tab ? classes["tab-selected"] : ""
              }`}
              onClick={() => handleTabSelection(tab)}
              key={idGen()}
            >
              {tab}
            </li>
          ))}
        </ul>
        <h3 className={classes["text-title"]}>Varius tempor.</h3>
        <p className={classes.text}>{tabContent}</p>
        <h3 className={classes["details-title"]}>More details</h3>
        <ul className={classes["details-list"]}>
          {Array.from({ length: 5 }, () => (
            <li className={classes["details-list-item"]} key={idGen()}>
              <SvgIcon name="icon_checkmark2" width={24} height={25} />
              <span>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac
                quam dolor. In dignissim lectus sed nisl tempor, ac porttitor
              </span>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default Tabs;
