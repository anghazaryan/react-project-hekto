import { useNavigate } from "react-router-dom";
import empty from "../../assets/img/empty.jpg";
import Button from "../../components/Button/Button";
import classes from "./CartEmptyPage.module.css";

const CartEmptyPage = () => {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/products");
    };
    return (
        <section className={classes["cart-empty"]}>
            <div className={classes.container}>
                <img src={empty} alt="Empty cart photo" width={387} height={286} />
                <h3 className={classes.title}>Your cart is empty</h3>
                <Button
                    name="Start Shopping"
                    size="medium"
                    className={classes.btn}
                    onClick={handleClick}
                />
            </div>
        </section>
    );
};

export default CartEmptyPage;
