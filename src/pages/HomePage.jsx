import FeaturedProducts from "../components/Sections/FeaturedProducts/FeaturedProducts";
import Hero from "../components/Sections/Hero/Hero";
import LatestProducts from "../components/Sections/LatestProducts/LatestProducts";
import TopCategories from "../components/Sections/TopCategories/TopCategories";
import TrendingProducts from "../components/Sections/TrendingProducts/TrendingProducts";
import UniqueFeatures from "../components/Sections/UniqueFeatures/UniqueFeatures";
import Discount from "../components/Sections/Discount/Discount";
import Newsletter from "../components/Sections/Newsletter/Newsletter";
import LatestBlog from "../components/Sections/LatestBlog/LatestBlog";

const HomePage = () => {
  return (
    <>
      <Hero />
      <FeaturedProducts />
      <LatestProducts />
      <UniqueFeatures />
      <TrendingProducts />
      <Discount />
      <TopCategories />
      <Newsletter />
      <LatestBlog />
    </>
  );
};

export default HomePage;
