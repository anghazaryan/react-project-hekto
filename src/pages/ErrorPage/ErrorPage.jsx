import { useRouteError } from "react-router-dom";
import Header from "../../layout/Header/Header";
import Footer from "../../layout/Footer/Footer";
import classes from "./ErrorPage.module.css";

const ErrorPage = () => {
    const error = useRouteError();

    let title = 'An error occurred!';
    let message = 'Something went wrong!';

    if (error.status === 500) {
        message = JSON.parse(error.data).message;
    }

    if (error.status === 404) {
        title = 'Not found!';
        message = 'Could not find resource or page.';
    }

    return (
        <>
            <Header />
            <div className={classes.container}>
                <h1 className={classes.title} >{title}</h1>
                <h3 className={classes.message}>{message}</h3>
            </div>
            <Footer />
        </>
    );
}

export default ErrorPage;