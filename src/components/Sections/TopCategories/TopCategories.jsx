import { useContext } from "react";
import Card4 from "../../Cards/Card4/Card4";
import SvgIcon from "../../SvgIcon";
import classes from "./TopCategories.module.css";
import ProductContext from "../../../store/ProductContext";

const TopCategories = () => {
  const productCtx = useContext(ProductContext);

  const products = productCtx.fetchedProducts
    .filter((product) => product.soldTimes >= 1700)
    .slice(4, 8);

  return (
    <section className={classes["section-top"]}>
      <h2 className={classes.heading}>Top Categories</h2>
      <div className={classes.products}>
        {products.map((product) => (
          <Card4 product={product} key={product.id} />
        ))}
      </div>
      <div className={classes.dots}>
        <SvgIcon
          name="icon_dot_active"
          width={8}
          height={8}
          className={classes.dot}
        />
        <SvgIcon
          name="icon_dot"
          width={10}
          height={10}
          className={classes.dot}
        />
        <SvgIcon
          name="icon_dot"
          width={10}
          height={10}
          className={classes.dot}
        />
      </div>
    </section>
  );
};

export default TopCategories;
