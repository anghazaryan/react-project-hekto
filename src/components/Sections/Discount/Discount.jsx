import idGen from "../../../utils/idGenerator";
import SvgIcon from "../../SvgIcon";
import Button from "../../Button/Button";
import headphones from "../../../assets/img/headphones2.png";
import classes from "./Discount.module.css";
import { useNavigate } from "react-router-dom";
import { DISCOUNT_LIST_ITEMS } from "../../../utils/constants";

const Discount = () => {
  const navigate = useNavigate();

  const handleShopBtnClick = () => {
    navigate("products");
  };
  return (
    <section className={classes["section-discount"]}>
      <h2 className={classes.heading}>Discount Item</h2>
      <nav className={classes.nav}>
        <ul className={classes.list}>
          <li className={classes["list-item"]}>
            <a href="#">Headphones</a>
          </li>
          <li className={classes["list-item"]}>
            <a href="#">Laptop</a>
          </li>
          <li className={classes["list-item"]}>
            <a href="#">Other</a>
          </li>
        </ul>
      </nav>
      <div className={classes.content}>
        <div className={classes.description}>
          <h3 className={classes.discount}>20% Discount Of All Products</h3>
          <p className={classes.title}>Headphones Compact</p>
          <p className={classes.text}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu eget
            feugiat habitasse nec, bibendum condimentum.
          </p>
          <div className={classes["checkmark-list"]}>
            {DISCOUNT_LIST_ITEMS.map((item) => (
              <div className={classes["checkmark-list-item"]} key={idGen()}>
                <SvgIcon
                  name="icon_checkmark2"
                  width={24}
                  height={25}
                  className={classes.checkmark}
                />
                <p className={classes["checkmark-list-item-text"]}>{item}</p>
              </div>
            ))}
          </div>
          <Button
            name="Shop Now"
            size="medium"
            className={classes.btn}
            onClick={handleShopBtnClick}
          />
        </div>
        <img src={headphones} alt="Headphones" width={574} height={511} />
      </div>
    </section>
  );
};

export default Discount;
