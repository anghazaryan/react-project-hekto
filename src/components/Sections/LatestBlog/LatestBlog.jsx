import { useContext } from "react";
import Card5 from "../../Cards/Card5/Card5";
import classes from "./LatestBlog.module.css";
import ProductContext from "../../../store/ProductContext";

const LatestBlog = () => {
  const productCtx = useContext(ProductContext);

  const products = productCtx.fetchedProducts
    .filter((product) => product.brand === "IKEA")
    .slice(1, 4);

  return (
    <section className={classes["section-latest-blog"]}>
      <h2 className={classes.heading}>Latest Blog</h2>
      <div className={classes.products}>
        {products.map((product) => (
          <Card5 product={product} key={product.id} />
        ))}
      </div>
    </section>
  );
};

export default LatestBlog;
