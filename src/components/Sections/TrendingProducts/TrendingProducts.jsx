import { useContext } from "react";
import Card3 from "../../Cards/Card3/Card3";
import classes from "./TrendingProducts.module.css";
import ProductContext from "../../../store/ProductContext";

const TrendingProducts = () => {
  const productCtx = useContext(ProductContext);
  const products = productCtx.fetchedProducts
    .filter((product) => Math.round(product.rating.value) >= 5)
    .slice(0, 4);

  return (
    <section className={classes["section-ternding"]}>
      <h2 className={classes.heading}>Trending Products</h2>
      <div className={classes.products}>
        {products.map((product) => (
          <Card3 product={product} key={product.id} />
        ))}
      </div>
    </section>
  );
};

export default TrendingProducts;
