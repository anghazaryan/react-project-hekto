import sofa from "../../../assets/img/sofa.png";
import Button from "../../Button/Button";
import SvgIcon from "../../SvgIcon";
import classes from "./UniqueFeatures.module.css";

const UniqueFeatures = () => {
  return (
    <section className={classes["section-unique"]}>
      <img src={sofa} alt="Sofa" width={576} height={518} />
      <div className={classes.content}>
        <h2 className={classes.heading}>
          Unique Features Of leatest & Trending Poducts
        </h2>
        <ul className={classes.list}>
          <li className={classes["list-item"]}>
            <SvgIcon
              name="icon_circle"
              width={15}
              height={15}
              className={classes["icon-1"]}
            />
            <span className={classes.text}>
              All frames constructed with hardwood solids and laminates
            </span>
          </li>
          <li className={classes["list-item"]}>
            <SvgIcon
              name="icon_circle"
              width={15}
              height={15}
              className={classes["icon-2"]}
            />
            <span className={classes.text}>
              Reinforced with double wood dowels, glue, screw - nails corner
            </span>
          </li>
          <li className={classes["list-item"]}>
            <SvgIcon
              name="icon_circle"
              width={15}
              height={15}
              className={classes["icon-3"]}
            />
            <span className={classes.text}>
              Arms, backs and seats are structurally reinforced
            </span>
          </li>
        </ul>
        <div className={classes["btn-container"]}>
          <Button name="Add to Cart" size="medium" className={classes.btn} />
          <div className={classes["product-description"]}>
            <span>B&B Italian Sofa</span>
            <span>$32.00</span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default UniqueFeatures;
