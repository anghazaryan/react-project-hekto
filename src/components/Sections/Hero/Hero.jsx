import Button from "../../Button/Button";
import SvgIcon from "../../SvgIcon";
import classes from "./Hero.module.css";
import lamp from "../../../assets/img/lamp.png";
import { useNavigate } from "react-router-dom";

const Hero = () => {
  const navigate = useNavigate();

  const handleShopBtnClick = () => {
    navigate("products");
  };
  return (
    <section className={classes.hero}>
      <div className={classes.container}>
        <div className={classes["hero-left"]}>
          <img src={lamp} alt="Lamp" width={380} height={380} />
          <SvgIcon
            name="icon_circle"
            className={classes["icon-circle"]}
            width={15}
            height={15}
          />
        </div>
        <div className={classes.content}>
          <p className={classes.title}>Best Headphones For Your Life....</p>
          <h1 className={classes.heading}>New Trendy Collection Headphones</h1>
          <p className={classes.text}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Magna in
            est adipiscing in phasellus non in justo.
          </p>
          <Button
            name="Shop now"
            size="medium"
            className={classes.btn}
            onClick={handleShopBtnClick}
          />
        </div>
        <div className={classes["hero-right"]}>
          <div
            className={classes.headphones}
            style={{ backgroundImage: `url(src/assets/img/headphones.jpg)` }}
          ></div>
          <div
            className={classes.discount}
            style={{ backgroundImage: `url(src/assets/img/discountBg.png)` }}
          >
            <p>
              50% <br /> Off
            </p>
          </div>
        </div>
      </div>
      <SvgIcon
        name="icon_diamond"
        width={85}
        height={19}
        className={classes["icon-diamond"]}
      />
    </section>
  );
};

export default Hero;
