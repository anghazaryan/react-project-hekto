import PropTypes from "prop-types";
import { useContext } from "react";
import idGen from "../../../utils/idGenerator";
import Card1 from "../../Cards/Card1/Card1";
import classes from "./RelatedProducts.module.css";
import ProductContext from "../../../store/ProductContext";

const RelatedProducts = ({ product }) => {
  const productCtx = useContext(ProductContext);

  const products = productCtx.fetchedProducts
    .filter((item) => item.brand === product.brand)
    .slice(0, 3);

  return (
    <section className={classes["section-related"]}>
      <h2 className={classes.heading}>Related Products</h2>
      <div className={classes.products}>
        {products.map((item) => (
          <Card1 product={item} key={idGen()} />
        ))}
      </div>
    </section>
  );
};

RelatedProducts.propTypes = {
  product: PropTypes.shape({
    brand: PropTypes.string,
  }),
};

export default RelatedProducts;
