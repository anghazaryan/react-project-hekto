import Button from "../../Button/Button";
import sofa from "../../../assets/img/sofa2.jpg";
import classes from "./Newsletter.module.css";

const Newsletter = () => {
  return (
    <section
      className={classes["section-news"]}
      style={{ backgroundImage: `url(${sofa})` }}
    >
      <h2 className={classes.heading}>
        Get Latest Update By Subscribe 0ur Newsletter
      </h2>
      <Button name="Subscribe" size="medium" className={classes.btn} />
    </section>
  );
};

export default Newsletter;
