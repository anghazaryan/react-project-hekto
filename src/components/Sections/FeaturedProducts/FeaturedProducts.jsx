import { useContext } from "react";
import idGen from "../../../utils/idGenerator";
import Card1 from "../../Cards/Card1/Card1";
import SvgIcon from "../../SvgIcon";
import classes from "./FeaturedProducts.module.css";
import ProductContext from "../../../store/ProductContext";

const FeaturedProducts = () => {
  const productCtx = useContext(ProductContext);

  const products = productCtx.fetchedProducts.slice(0, 4);

  return (
    <section className={classes["featured-products"]}>
      <h2 className={classes.heading}>Featured Products</h2>
      <div className={classes.products}>
        {products.map((product) => (
          <Card1 product={product} key={product.id} />
        ))}
      </div>
      <div className={classes.icons}>
        <SvgIcon
          name="icon_dash_active"
          width={24}
          height={4}
          className={classes["icon-active"]}
        />
        {Array.from({ length: 3 }, () => (
          <SvgIcon
            name="icon_dash"
            width={24}
            height={4}
            className={classes.icon}
            key={idGen()}
          />
        ))}
      </div>
    </section>
  );
};

export default FeaturedProducts;
