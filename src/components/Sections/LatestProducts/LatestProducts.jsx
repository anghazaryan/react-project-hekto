import { useContext } from "react";
import idGen from "../../../utils/idGenerator";
import Card2 from "../../Cards/Card2/Card2";
import classes from "./LatestProducts.module.css";
import ProductContext from "../../../store/ProductContext";
import { LATESTPROD_NAVLIST } from "../../../utils/constants";

const LatestProducts = () => {
  const productCtx = useContext(ProductContext);
  const products = productCtx.fetchedProducts.slice(4, 10);

  return (
    <section className={classes.latest}>
      <h2 className={classes.heading}>Latest Products</h2>
      <nav className={classes.nav}>
        <ul className={classes.list}>
          {LATESTPROD_NAVLIST.map((item) => (
            <li className={classes["list-item"]} key={idGen()}>
              <a>{item}</a>
            </li>
          ))}
        </ul>
      </nav>
      <div className={classes.products}>
        {products.map((product) => (
          <Card2 product={product} key={product.id} />
        ))}
      </div>
    </section>
  );
};

export default LatestProducts;
