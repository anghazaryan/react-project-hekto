import PropTypes from "prop-types";
import idGen from "../../utils/idGenerator";
import Button from "../Button/Button";
import classes from "./Pagination.module.css";

const Pagination = ({ postsPerPage, totalPosts, currentPage, paginate }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); ++i) {
    pageNumbers.push(i);
  }

  return (
    <ul className={classes.pagination}>
      {pageNumbers.map((number) => (
        <li className={classes["page-number"]} key={idGen()}>
          <Button
            name={number}
            size="small"
            onClick={() => paginate(number)}
            className={`${classes.btn} ${
              number === currentPage ? classes.active : ""
            }`}
          />
        </li>
      ))}
    </ul>
  );
};

Pagination.propTypes = {
  postsPerPage: PropTypes.number,
  totalPosts: PropTypes.number,
  currentPage: PropTypes.number,
  paginate: PropTypes.func,
};

export default Pagination;
