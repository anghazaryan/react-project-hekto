import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import SvgIcon from "../SvgIcon";
import classes from "./CustomCheckbox.module.css";

const colorTypes = {
  "var(--info-color)": "var(--info-light-color)",
  "var(--secondary-color)": "var(--secondary-light-color)",
  "var(--primary-color)": "var(--primary-more-light-color)",
  "var(--success-color)": "var(--success-light-color)",
};

const CustomCheckbox = ({ id, label, color, checked, onCheck, onUncheck }) => {
  const [isChecked, setIsChecked] = useState(checked);
  const notCheckedColor = colorTypes[color];

  useEffect(() => {
    setIsChecked(checked);
  }, [checked]);

  const handleClick = () => {
    setIsChecked((isChecked) => !isChecked);

    isChecked ? onUncheck() : onCheck();
  };

  return (
    <div className={classes["checkbox-wrapper"]}>
      <input
        type="checkbox"
        className={classes["checkbox-input"]}
        id={id}
        onChange={handleClick}
      />
      <div
        className={classes.checkbox}
        style={{ backgroundColor: isChecked ? color : notCheckedColor }}
        onClick={handleClick}
      >
        {isChecked && (
          <SvgIcon
            name="icon_checkmark"
            width={16}
            height={16}
            className={classes.icon}
          />
        )}
      </div>
      <label className={classes.label} htmlFor={id}>
        {label}
      </label>
    </div>
  );
};

CustomCheckbox.propTypes = {
  id: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  color: PropTypes.string,
  checked: PropTypes.bool,
  onCheck: PropTypes.func,
  onUncheck: PropTypes.func,
};

export default CustomCheckbox;
