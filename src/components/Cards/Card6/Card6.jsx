import PropTypes from "prop-types";
import { useContext } from "react";
import { currencyFormatter } from "../../../utils/fortmatting";
import Button from "../../Button/Button";
import SvgIcon from "../../SvgIcon";
import classes from "./Card6.module.css";
import CartContext from "../../../store/CartContext";
import { useNavigate } from "react-router-dom";
import RatingStars from "../../RatingStars/RatingStars";

const Card6 = ({ product, type }) => {
  const cartCtx = useContext(CartContext);
  const navigate = useNavigate();

  const {
    id,
    thumbnail: img,
    name: title,
    price: discountPrice,
    wasPrice: price,
    description: text,
    rating: { value: rating },
  } = product;

  const handleAddProductToCart = () => {
    cartCtx.addItem(product);
  };

  const handleProdClick = (productId) => {
    navigate(`/products/${productId}`);
  };

  return (
    <div className={`${classes.card} ${classes[`card-${type}`]}`}>
      <div
        className={classes["img-container"]}
        onClick={() => handleProdClick(id)}
      >
        <img
          src={img}
          width={288}
          height={200}
          alt={title}
          className={classes.img}
        />
      </div>
      <div className={classes[`description-${type}`]}>
        <div className={classes[`title-and-stars-${type}`]}>
          <p className={classes.title}>{title}</p>
          <RatingStars rating={rating} />
        </div>
        <div className={`${classes.prices} ${classes[`prices-${type}`]}`}>
          <span className={classes.discountPrice}>
            {currencyFormatter.format(discountPrice)}
          </span>
          <span className={classes.price}>
            {currencyFormatter.format(price)}
          </span>
        </div>
        <p className={`${classes.text} ${classes[`text-${type}`]}`}>{text}</p>
        <div className={classes.icons}>
          <Button
            size="circle"
            className={classes["icon-btn"]}
            onClick={handleAddProductToCart}
          >
            <SvgIcon
              name="icon_cart"
              width={16}
              height={16}
              className={classes["icon"]}
            />
          </Button>
          <Button size="circle" className={classes["icon-btn"]}>
            <SvgIcon
              name="icon_heart"
              width={16}
              height={16}
              className={classes["icon"]}
            />
          </Button>
          <Button size="circle" className={classes["icon-btn"]}>
            <SvgIcon
              name="icon_zoomIn"
              width={16}
              height={16}
              className={classes["icon"]}
            />
          </Button>
        </div>
      </div>
    </div>
  );
};

Card6.propTypes = {
  type: PropTypes.string,
  product: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    thumbnail: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    wasPrice: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    description: PropTypes.string,
    rating: PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    }),
  }),
};

export default Card6;
