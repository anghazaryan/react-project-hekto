import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import classes from "./Card3.module.css";
import { currencyFormatter } from "../../../utils/fortmatting";

const Card3 = ({ product }) => {
  const navigate = useNavigate();

  const {
    id,
    thumbnail: img,
    name: title,
    price: discountPrice,
    wasPrice: price,
  } = product;

  const handleProdClick = (productId) => {
    navigate(`products/${productId}`);
  };

  return (
    <div className={classes.card}>
      <div
        className={classes["img-container"]}
        onClick={() => handleProdClick(id)}
      >
        <img src={img} width={272} height={232} alt={title} />
      </div>

      <div className={classes.description}>
        <p className={classes.title}>{title}</p>
        <div className={classes.prices}>
          <span className={classes.discountPrice}>
            {currencyFormatter.format(discountPrice)}
          </span>
          <span className={classes.price}>
            {currencyFormatter.format(price)}
          </span>
        </div>
      </div>
    </div>
  );
};

Card3.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    thumbnail: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    wasPrice: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
};

export default Card3;
