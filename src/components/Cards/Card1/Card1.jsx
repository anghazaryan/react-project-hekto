import { useState } from "react";
import PropTypes from "prop-types";
import Button from "../../Button/Button";
import SvgIcon from "../../SvgIcon";
import classes from "./Card1.module.css";
import useCart from "../../../hooks/handleCartIconClick";
import { useNavigate } from "react-router-dom";
import { currencyFormatter } from "../../../utils/fortmatting";

const Card1 = ({ product }) => {
  const [hovered, setHovered] = useState(false);
  const { handleCartIconClick } = useCart();
  const navigate = useNavigate();

  const { id, thumbnail: img, name: title, code, price } = product;

  const handleMouseEnter = () => {
    setHovered(true);
  };

  const handleMouseLeave = () => {
    setHovered(false);
  };

  const handleProdClick = (productId) => {
    navigate(`/products/${productId}`);
  };

  return (
    <div
      className={classes.card}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className={classes["img-container"]}>
        <img
          src={img}
          width={304}
          height={232}
          alt={title}
          className={classes.img}
        />
        {hovered && (
          <div className={classes.icons}>
            <Button size="circle" className={classes["icon-btn"]}>
              <SvgIcon
                name="icon_cart"
                width={16}
                height={16}
                className={classes["icon-svg"]}
                onClick={() => handleCartIconClick(product)}
              />
            </Button>
            <Button size="circle" className={classes["icon-btn"]}>
              <SvgIcon
                name="icon_heart"
                width={16}
                height={16}
                className={classes["icon-svg"]}
              />
            </Button>
            <Button size="circle" className={classes["icon-btn"]}>
              <SvgIcon
                name="icon_zoomIn"
                width={16}
                height={16}
                className={classes["icon-svg"]}
              />
            </Button>
          </div>
        )}
        {hovered && (
          <Button
            name="View details"
            size="medium"
            className={classes["detail-btn"]}
            onClick={() => handleProdClick(id)}
          />
        )}
      </div>
      <div className={classes.description}>
        <p className={classes.title}>{title}</p>
        <p className={classes.code}>{`Code - ${code}`}</p>
        <p className={classes.price}>{currencyFormatter.format(price)}</p>
      </div>
    </div>
  );
};

Card1.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    thumbnail: PropTypes.string,
    name: PropTypes.string,
    code: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
};

export default Card1;
