import { useState } from "react";
import PropTypes from "prop-types";
import Button from "../../Button/Button";
import classes from "./Card4.module.css";

const Card4 = ({ product }) => {
  const [hovered, setHovered] = useState(false);

  const { thumbnail: img, name: title } = product;

  const handleMouseEnter = () => {
    setHovered(true);
  };

  const handleMouseLeave = () => {
    setHovered(false);
  };

  return (
    <div
      className={classes.card}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className={classes["img-container"]}>
        <img
          src={img}
          width={272}
          height={272}
          alt={title}
          className={classes.img}
        />
        {hovered && (
          <Button
            name="View Category"
            size="medium"
            className={classes["btn"]}
          />
        )}
      </div>
      <div className={classes["title"]}>{title}</div>
    </div>
  );
};

Card4.propTypes = {
  product: PropTypes.shape({
    thumbnail: PropTypes.string,
    name: PropTypes.string,
  }),
};

export default Card4;
