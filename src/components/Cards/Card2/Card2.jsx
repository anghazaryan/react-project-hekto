import { useState } from "react";
import PropTypes from "prop-types";
import Button from "../../Button/Button";
import SvgIcon from "../../SvgIcon";
import classes from "./Card2.module.css";
import useCart from "../../../hooks/handleCartIconClick";
import { currencyFormatter } from "../../../utils/fortmatting";

const Card2 = ({ product }) => {
  const [hovered, setHovered] = useState(false);
  const { handleCartIconClick } = useCart();

  const {
    thumbnail: img,
    name: title,
    price: discountPrice,
    wasPrice: price,
  } = product;

  const handleMouseEnter = () => {
    setHovered(true);
  };

  const handleMouseLeave = () => {
    setHovered(false);
  };

  return (
    <div
      className={classes.card}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div
        className={classes["img-container"]}
        style={{ backgroundImage: `url(${img})` }}
      >
        <img
          src={img}
          width={416}
          height={272}
          alt={title}
          className={classes.img}
        />
        {hovered && (
          <div className={classes.icons}>
            <Button size="circle" className={classes["icon-btn"]}>
              <SvgIcon
                name="icon_cart"
                width={16}
                height={16}
                className={classes["icon-svg"]}
                onClick={() => handleCartIconClick(product)}
              />
            </Button>
            <Button size="circle" className={classes["icon-btn"]}>
              <SvgIcon
                name="icon_heart"
                width={16}
                height={16}
                className={classes["icon-svg"]}
              />
            </Button>
            <Button size="circle" className={classes["icon-btn"]}>
              <SvgIcon
                name="icon_zoomIn"
                width={16}
                height={16}
                className={classes["icon-svg"]}
              />
            </Button>
          </div>
        )}
      </div>
      <div className={classes.description}>
        <span className={classes.title}>{title}</span>
        <span>{currencyFormatter.format(discountPrice)}</span>
        <span className={classes.price}>{currencyFormatter.format(price)}</span>
      </div>
    </div>
  );
};

Card2.propTypes = {
  product: PropTypes.shape({
    thumbnail: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    wasPrice: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
};

export default Card2;
