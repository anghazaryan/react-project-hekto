import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { dateFormater } from "../../../utils/fortmatting";
import SvgIcon from "../../SvgIcon";
import classes from "./Card5.module.css";

const Card5 = ({ product }) => {
  const navigate = useNavigate();

  const {
    id,
    thumbnail: img,
    brand: owner,
    createdAt: date,
    name: title,
    description: text,
  } = product;

  const handleProdClick = (productId) => {
    navigate(`products/${productId}`);
  };
  return (
    <div className={classes.card}>
      <div className={classes["img-container"]}>
        <img src={img} width={416} height={255} alt={title} />
      </div>
      <div className={classes.description}>
        <div className={classes["owner-and-date"]}>
          <div className={classes["owner-date-item"]}>
            <SvgIcon
              name="icon_pen"
              width={20}
              height={20}
              className={classes.icon}
            />
            <span>{owner}</span>
          </div>
          <div className={classes["owner-date-item"]}>
            <SvgIcon
              name="icon_calendar"
              width={18}
              height={18}
              className={classes.icon}
            />
            <span>{dateFormater(date)}</span>
          </div>
        </div>
        <p className={classes.title}>{title}</p>
        <p className={classes.text}>{text}</p>
        <a
          href="#"
          className={classes.more}
          onClick={() => handleProdClick(id)}
        >
          Read More
        </a>
      </div>
    </div>
  );
};

Card5.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    thumbnail: PropTypes.string,
    name: PropTypes.string,
    brand: PropTypes.string,
    createdAt: PropTypes.string,
    description: PropTypes.string,
  }),
};

export default Card5;
