import PropTypes from "prop-types";
import SvgIcon from "../SvgIcon";
import idGen from "../../utils/idGenerator";
import { ratingCounter } from "../../utils/ratingCounter";
import classes from "./RatingStars.module.css";

const RatingStars = ({ rating, className }) => {
  const [ratedStars, notRatedStars] = ratingCounter(rating);
  return (
    <div className={`${classes["star-icons"]} ${className ? className : ""}`}>
      {Array.from({ length: ratedStars }, () => (
        <SvgIcon
          name="icon_star"
          width={16}
          height={16}
          className={classes["icon-star"]}
          key={idGen()}
        />
      ))}
      {notRatedStars >= 0 &&
        Array.from({ length: notRatedStars }, () => (
          <SvgIcon
            name="icon_star"
            width={16}
            height={16}
            key={idGen()}
            className={classes["icon-not-rated"]}
          />
        ))}
    </div>
  );
};

RatingStars.propTypes = {
  rating: PropTypes.number,
  className: PropTypes.string,
};

export default RatingStars;
