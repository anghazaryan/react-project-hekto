import PropTypes from "prop-types";
import { useEffect, useRef, useState } from "react";
import idGen from "../../utils/idGenerator";
import SvgIcon from "../SvgIcon";
import classes from "./CustomSelect.module.css";

const CustomSelect = ({
  dropdownList,
  className,
  onItemClick,
  selected,
  width,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(selected);
  const selectRef = useRef(null);

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (selectRef.current && !selectRef.current.contains(event.target)) {
        setIsOpen(false);
      }
    };

    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, []);

  const toggle = () => {
    setIsOpen((isOpen) => !isOpen);
  };

  const selectItem = (item) => {
    setSelectedItem(item);
    setIsOpen(false);
    onItemClick(item);
  };

  const cssClass = `${classes.select} ${className ? className : ""}`;

  return (
    <div
      className={cssClass}
      style={{ minWidth: width ? width : "" }}
      ref={selectRef}
    >
      <div className={classes.selected} onClick={toggle}>
        <span>{selectedItem}</span>
        <SvgIcon
          name={`icon_arrow_${isOpen ? "up" : "down"}`}
          width={16}
          height={16}
        />
      </div>
      <ul
        className={`${classes["select-dropdown"]} ${
          isOpen ? "" : classes.hidden
        }`}
      >
        {dropdownList.map((item) => (
          <li
            className={classes["select-item"]}
            key={idGen()}
            onClick={() => selectItem(item)}
          >
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
};

CustomSelect.propTypes = {
  dropdownList: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  ),
  className: PropTypes.string,
  onItemClick: PropTypes.func,
  selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.string,
};

export default CustomSelect;
