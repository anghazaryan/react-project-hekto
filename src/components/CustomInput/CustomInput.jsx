import PropTypes from "prop-types";
import classes from "./CustomInput.module.css";

const CustomInput = ({
  id,
  type,
  placeholder,
  value,
  className,
  children,
  onInputChange,
  ...props
}) => {
  const cssClass = `${classes["input-wrapper"]} ${className ? className : ""}`;

  return (
    <div className={cssClass}>
      <input
        type={type}
        placeholder={placeholder}
        className={classes.input}
        id={id}
        value={value}
        onChange={onInputChange}
        {...props}
      />
      {children}
    </div>
  );
};

CustomInput.propTypes = {
  id: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.element,
  onInputChange: PropTypes.func,
};

export default CustomInput;
