import PropTypes from "prop-types";
import classes from "./NumberInput.module.css";
import SvgIcon from "../SvgIcon";

const NumberInput = ({ count, className, onMinusClick, onPlusClick }) => {
  const cssClass = `${classes["number-input"]} ${className ? className : ""}`;

  return (
    <div className={cssClass}>
      <SvgIcon
        name="icon_minus"
        width={8}
        height={6}
        onClick={onMinusClick}
        className={classes.icon}
      />
      {count}
      <SvgIcon
        name="icon_plus"
        width={12}
        height={12}
        onClick={onPlusClick}
        className={classes.icon}
      />
    </div>
  );
};

NumberInput.propTypes = {
  count: PropTypes.number,
  className: PropTypes.string,
  onMinusClick: PropTypes.func,
  onPlusClick: PropTypes.func,
};

export default NumberInput;
