import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./pages/Root";
import HomePage from "./pages/HomePage";
import ProductListerPage from "./pages/ProductsLister/ProductListerPage";
import ProductDetailsPage from "./pages/ProductDetails/ProductDetailsPage";
import CartPage from "./pages/CartPage/CartPage";
import CartEmptyPage from "./pages/CartEmpty/CartEmptyPage";
import { CartContextProvider } from "./store/CartContext";
import { ProductContextProvider } from "./store/ProductContext";
import ErrorPage from "./pages/ErrorPage/ErrorPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      { path: "products", element: <ProductListerPage /> },
      { path: "products/:id", element: <ProductDetailsPage /> },
      { path: "cart", element: <CartPage /> },
      { path: "cartEmpty", element: <CartEmptyPage /> },
    ],
  },
]);

const App = () => {
  return (
    <ProductContextProvider>
      <CartContextProvider>
        <RouterProvider router={router} />
      </CartContextProvider>
    </ProductContextProvider>
  );
};

export default App;
