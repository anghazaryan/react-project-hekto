const STARS = 5;

export const ratingCounter = (rating) => {
  const ratedStars = Math.round(rating);
  const notRatedStars = STARS - ratedStars;

  return [ratedStars, notRatedStars];
};
