export const resetFilters = (filters) => {
  const resetFilters = { ...filters };

  for (let filterKey in filters) {
    resetFilters[filterKey].values = [...filters[filterKey].values];
    resetFilters[filterKey].values.forEach((value) => {
      value.isChecked = false;
    });
  }

  return resetFilters;
};
