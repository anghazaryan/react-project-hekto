export const FOOTER_ITEMS = {
  Categories: [
    "Laptops & Computers",
    "Cameras & Photography",
    "Smart Phones & Tablets",
    "Video Games & Consoles",
    "Waterproof Headphones",
  ],
  "Customer Care": [
    "My Account",
    "Discount",
    "Returns",
    "Orders History",
    "Order Tracking",
  ],
  Pages: [
    "Blog",
    "Browse the Shop",
    "Category",
    "Pre-Built Pages",
    "Visual Composer Elements",
  ],
};

export const TAB_CONTENT = {
  Description:
    "Description - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac quamdolor.",
  "Aditional Info":
    "additional info - Pellentesque diam dolor, tincidunt nec ante congue,",
  Reviews:
    "reviews - Phasellus quis sodales augue. Donec necultricies diam. Integer feugiat",
  Video: "video - Mauris ullamcorper quis nisl sed dictum.",
};

export const PAGE_LIST = [10, 15, 20];

export const SORT_LIST = ["None", "Price: High -> Low", "Price: Low -> High"];

export const LATESTPROD_NAVLIST = [
  "New Arrival",
  "Best Seller",
  "Featured",
  "Special Offer",
];

export const DISCOUNT_LIST_ITEMS = [
  "Material expose like metals",
  "Clear lines and geomatric figures",
  "Simple neutral colours.",
  "Material expose like metals",
];
