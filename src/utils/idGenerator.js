const idGen = () => {
  return `${Date.now()}${Math.random() * 1000}`;
};

export default idGen;
