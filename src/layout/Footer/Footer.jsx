import SvgIcon from "../../components/SvgIcon";
import classes from "./Footer.module.css";
import List from "./List";
import logo from "../../assets/logo.svg";
import CustomInput from "../../components/CustomInput/CustomInput";
import Button from "../../components/Button/Button";
import { FOOTER_ITEMS } from "../../utils/constants";
import idGen from "../../utils/idGenerator";

const Footer = () => {
  return (
    <footer className={classes.footer}>
      <div className={classes["footer-top"]}>
        <div className={`${classes["sign-up"]} ${classes["footer-item"]}`}>
          <img src={logo} alt="Logo" width={103} height={30} />
          <CustomInput
            type="email"
            placeholder="Enter Email Address"
            className={classes.input}
            id="email"
          >
            <Button name="Sign up" size="small" className={classes.btn} />
          </CustomInput>
          <p className={classes.address}>
            17 Princess Road, London, Greater London NW1 8JR, UK
          </p>
        </div>

        {Object.keys(FOOTER_ITEMS).map((item) => (
          <div className={classes["footer-item"]} key={idGen()}>
            <List title={item} list={FOOTER_ITEMS[item]} />
          </div>
        ))}
      </div>
      <div className={classes["footer-copyright"]}>
        <p className={classes.copyright}>&#169;Webecy - All Rights Reserved</p>
        <div className={classes.icons}>
          <a href="#">
            <SvgIcon
              name="icon_facebook"
              width={24}
              height={24}
              className={classes.icon}
            />
          </a>
          <a href="#">
            <SvgIcon
              name="icon_twitter"
              width={24}
              height={24}
              className={classes.icon}
            />
          </a>
          <a href="#">
            <SvgIcon
              name="icon_instagram"
              width={24}
              height={24}
              className={classes.icon}
            />
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
