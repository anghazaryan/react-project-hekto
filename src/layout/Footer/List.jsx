import PropTypes from "prop-types";
import idGen from "../../utils/idGenerator";
import classes from "./List.module.css";

const List = ({ className, title, list }) => {
  const cssClass = `${classes.ul} ${className ? className : ""}`;
  return (
    <ul className={cssClass}>
      <li className={classes.title}>
        <a href="#">{title}</a>
      </li>
      {list.map((el) => {
        return (
          <li key={idGen()} className={classes.item}>
            <a href="#">{el}</a>
          </li>
        );
      })}
    </ul>
  );
};

List.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.string),
};

export default List;
