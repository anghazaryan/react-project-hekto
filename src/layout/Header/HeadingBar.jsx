import SvgIcon from "../../components/SvgIcon";
import { useContext } from "react";
import CartContext from "../../store/CartContext";
import { useNavigate } from "react-router-dom";
import classes from "./HeadingBar.module.css";

const HeadingBar = () => {
  const cartCtx = useContext(CartContext);
  const navigate = useNavigate();

  const totalProductItems = cartCtx.items.reduce((num, item) => {
    return num + item.quantity;
  }, 0);

  const handleCartIconCLick = () => {
    if (totalProductItems === 0) {
      navigate("/cartEmpty");
    } else {
      navigate("/cart");
    }
  };
  return (
    <div className={`container ${classes["heading-bar"]}`}>
      <div className={classes["heading-bar-left"]}>
        <div className={classes["heading-bar-left-item"]}>
          <SvgIcon name="icon_email" width={16} height={16} />
          <a href="#">mhhasanul@gmail.com</a>
        </div>
        <div className={classes["heading-bar-left-item"]}>
          <SvgIcon name="icon_tel" width={16} height={16} />
          <a href="#">(12345)67890</a>
        </div>
      </div>
      <ul className={classes["heading-bar-right"]}>
        <li className={classes["heading-bar-right-item"]}>
          English
          <SvgIcon
            name="icon_arrow_down"
            width={16}
            height={16}
            className={classes["icon-arrow"]}
          />
        </li>
        <li className={classes["heading-bar-right-item"]}>
          USD
          <SvgIcon
            name="icon_arrow_down"
            width={16}
            height={16}
            className={classes["icon-arrow"]}
          />
        </li>
        <li className={classes["heading-bar-right-item"]}>
          Login
          <SvgIcon name="icon_signIn" width={16} height={16} />
        </li>
        <li className={classes["heading-bar-right-item"]}>
          Wishlist
          <SvgIcon name="icon_heart" width={16} height={16} />
        </li>
        <li
          className={classes["heading-bar-right-item"]}
          onClick={handleCartIconCLick}
        >
          {totalProductItems > 0 && (
            <span className={classes["cart-quantity"]}>
              {totalProductItems}
            </span>
          )}
          <SvgIcon name="icon_cart" width={16} height={16} />
        </li>
      </ul>
    </div>
  );
};

export default HeadingBar;
