import classes from "./Header.module.css";

import HeadingBar from "./HeadingBar";
import NavBar from "./NavBar";

const Header = () => {
  return (
    <div className={classes.header}>
      <HeadingBar />
      <NavBar />
    </div>
  );
};

export default Header;
