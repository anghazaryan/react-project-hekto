import logo from "../../assets/logo.svg";
import Button from "../../components/Button/Button";
import CustomInput from "../../components/CustomInput/CustomInput";
import SvgIcon from "../../components/SvgIcon";
import { NavLink, useNavigate } from "react-router-dom";
import { useContext, useState } from "react";
import ProductContext from "../../store/ProductContext";
import classes from "./NavBar.module.css";

const NavBar = () => {
  const productCtx = useContext(ProductContext);
  const [searchValue, setSearchValue] = useState("");
  const navigate = useNavigate();

  const handleLogoClick = () => {
    navigate("/");
  };

  const handleSearchInputChange = (event) => {
    setSearchValue(event.target.value);
  };

  const handleSearch = () => {
    navigate("/products");
    productCtx.findProducts(searchValue);
    setSearchValue("");
  };

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      handleSearch();
    }
  };

  return (
    <div className={classes["nav-bar"]}>
      <img
        src={logo}
        alt="Logo"
        className={classes.img}
        onClick={handleLogoClick}
      />
      <nav className={classes.nav}>
        <ul className={classes.list}>
          <li className={classes["list-item"]}>
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Home
            </NavLink>
          </li>
          <li className={classes["list-item"]}>
            <NavLink
              to="products"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Products
            </NavLink>
          </li>
          <li className={classes["list-item"]}>
            <NavLink
              to="blog"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Blog
            </NavLink>
          </li>
          <li className={classes["list-item"]}>
            <NavLink
              to="contact"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Contact
            </NavLink>
          </li>
        </ul>
      </nav>
      <CustomInput
        placeholder="Search"
        className={classes["nav-search"]}
        type="text"
        id="search"
        value={searchValue}
        onInputChange={handleSearchInputChange}
        onKeyPress={handleKeyPress}
      >
        <Button
          size="small"
          className={classes["btn-search"]}
          onClick={handleSearch}
        >
          <SvgIcon
            name="icon_search"
            width={32}
            height={32}
            className={classes["icon-search"]}
          />
        </Button>
      </CustomInput>
    </div>
  );
};

export default NavBar;
