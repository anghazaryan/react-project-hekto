import { useContext } from "react";
import CartContext from "../store/CartContext";

const useCart = () => {
  const cartCtx = useContext(CartContext);

  const handleCartIconClick = (product) => {
    cartCtx.addItem(product);
  };

  return { handleCartIconClick };
};

export default useCart;
